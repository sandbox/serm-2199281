<div id="page">
  <div id="page-top" class="clearfix">
    <?php if (!empty($page['search'])): ?>
      <div id="search-region">
        <?php print render($page['search']); ?>
      </div>
    <?php endif; ?>

    <?php if (!empty($user_links)): ?>
      <div id="user-links">
        <?php print $user_links; ?>
      </div>
    <?php endif; ?>

    <?php if (!empty($rss_contact) || !empty($today_date)): ?>
      <div id="today-rss-contact-wrapper" class="clearfix">
        <?php if (!empty($today_date)): ?>
          <div id="today-date">
            <?php print $today_date; ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($rss_contact)): ?>
          <div id="rss-contact">
            <?php print $rss_contact; ?>
          </div>
        <?php endif; ?>
      </div> <!-- /#today-rss-contact-wrapper -->
    <?php endif; ?>
  </div> <!-- /#page-top -->

  <div id="pre-header" class="clearfix">

    <?php if (!empty($logo)): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>

    <?php if (!empty($site_name) || !empty($site_slogan)): ?>
      <div id="name-and-slogan">
        <?php if (!empty($site_name)): ?>
          <?php if (!empty($title)): ?>
            <div id="site-name"><strong>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </strong></div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if (!empty($site_slogan)): ?>
          <div id="site-slogan">
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>
      </div> <!-- /#name-and-slogan -->
    <?php endif; ?>

  </div> <!-- /#pre-header -->

  <?php if (!empty($page['header']) || !empty($page['header_banner'])): ?>
    <div id="header-wrapper" class="clearfix">
      <?php if (!empty($page['header'])): ?>
        <div id="header">
          <?php print render($page['header']); ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($page['header_banner'])): ?>
        <?php print render($page['header_banner']); ?>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($main_menu)): ?>
    <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'clearfix')))); ?>
  <?php endif; ?>

  <div id="main" class="clearfix">

    <div id="content" class="column"><div class="section">
      <?php if (!empty($breadcrumb)): ?>
        <div id="breadcrumb"><?php print $breadcrumb; ?></div>
      <?php endif; ?>

      <?php if (!empty($messages)): ?>
        <?php print $messages; ?>
      <?php endif; ?>

      <?php if (!empty($page['banner_one'])): ?>
        <?php print render($page['banner_one']); ?>
      <?php endif; ?>

      <?php if (!empty($page['content_left']) || !empty($page['content_right'])): ?>
        <div id="content-left-right" class="clearfix">
          <?php if (!empty($page['content_left'])): ?>
            <?php print render($page['content_left']); ?>
          <?php endif; ?>

          <?php if (!empty($page['content_right'])): ?>
            <?php print render($page['content_right']); ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>

      <a id="main-content"></a>

      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <?php if (!empty($tabs)): ?>
        <div class="tabs">
          <?php print $tabs; ?>
        </div>
      <?php endif; ?>

      <?php print render($page['help']); ?>

      <?php print render($page['content']); ?>

      <?php if (!empty($page['banner_two'])): ?>
        <?php print render($page['banner_two']); ?>
      <?php endif; ?>

      <?php if (!empty($page['content_bottom_left']) || !empty($page['content_bottom_right'])): ?>
        <div id="content-bottom-left-right">
          <?php if (!empty($page['content_bottom_left'])): ?>
            <?php print render($page['content_bottom_left']); ?>
          <?php endif; ?>

          <?php if (!empty($page['content_bottom_right'])): ?>
            <?php print render($page['content_bottom_right']); ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div></div> <!-- /.section, /#content -->

    <?php if (!empty($page['sidebar_second'])): ?>
      <div id="sidebar-second" class="column sidebar">
        <?php print render($page['sidebar_second']); ?>
      </div> <!-- /#sidebar-second -->
    <?php endif; ?>

  </div> <!-- /#main -->

  <div id="footer">
    <?php if (!empty($developer_logo)): ?>
      <?php print $developer_logo; ?>
    <?php endif; ?>

    <?php if (!empty($secondary_menu)): ?>
      <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'inline', 'clearfix')))); ?>
    <?php endif; ?>
  </div> <!-- /#footer -->

  <?php if (!empty($copyright)): ?>
    <div id="copyright">
      <?php print $copyright; ?>
    </div>
  <?php endif; ?>

</div> <!-- /#page -->
