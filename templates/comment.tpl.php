<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="comment-top">
    <div class="submitted">
      <?php print $permalink; ?>
      <?php print $submitted; ?>
      <?php if ($new): ?>
        <span class="new"><?php print $new ?></span>
      <?php endif; ?>
    </div>
  </div>

  <?php print render($title_prefix); ?>
    <h3<?php print $title_attributes; ?>><?php print $title ?></h3>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php print $picture ?>

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php if ($signature): ?>
    <div class="user-signature clearfix">
      <?php print $signature ?>
    </div>
  <?php endif; ?>
</div>
<div class="comment-links clearfix">
  <?php print render($content['links']) ?>
</div>
