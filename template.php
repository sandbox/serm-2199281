<?php

/**
 * Preprocess variables for html.tpl.php.
 */
function vmoskve_preprocess_html(&$variables) {
  if (!empty($variables['page']['search'])) {
    $variables['classes_array'][] = 'search';
  }
  if (!empty($variables['page']['header_banner'])) {
    $variables['classes_array'][] = 'header-banner';
  }

  // If on an individual node page, add the 'page-node-view' to body classes.
  if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == NULL) {
    $variables['classes_array'][] = 'page-node-view';
  }
}

/**
 * Preprocess variables for page.tpl.php.
 */
function vmoskve_preprocess_page(&$variables) {
  $variables['user_links'] = _vmoskve_user_links();
  $variables['today_date'] = _vmoskve_today_date();
  $variables['rss_contact'] = _vmoskve_rss_contact($variables['feed_icons']);
  $variables['developer_logo'] = _vmoskve_developer_logo();

  $variables['tabs'] = render($variables['tabs']);

  $copyright = theme_get_setting('vmoskve_copyright');
  if (!empty($copyright['value'])) {
    $variables['copyright'] = check_markup($copyright['value'], $copyright['format']);
  }

  if (!theme_get_setting('vmoskve_front_page_content') && drupal_is_front_page()) {
    $variables['page']['content']['system_main']['#access'] = FALSE;
  }
  if (!theme_get_setting('vmoskve_front_page_title') && drupal_is_front_page()) {
    drupal_set_title('');
  }
}

/**
 * Process variables for page.tpl.php.
 */
function vmoskve_process_page(&$variables) {
  if (!theme_get_setting('vmoskve_page_breadcrumbs')) {
    $variables['breadcrumb'] = NULL;
  }
}

/**
 * Preprocess variables for node.tpl.php.
 */
function vmoskve_preprocess_node(&$variables) {
  $variables['title_attributes_array']['class'][] = 'node-title';

  if ($variables['display_submitted']) {
    $submitted = array();
    $submitted['date'] = array(
      '#prefix' => '<span class="post-date">',
      '#suffix' => '</span>',
      '#markup' => $variables['date'],
    );
    $submitted['author'] = array(
      '#markup' => $variables['name'],
    );
    $variables['submitted'] = render($submitted);
  }

  $variables['content']['links'] = render($variables['content']['links']);
}

/**
 * Preprocess variables for comment.tpl.php.
 */
function vmoskve_preprocess_comment(&$variables) {
  $node = $variables['elements']['#node'];
  $comment = $variables['elements']['#comment'];

  $variables['classes_array'][] = $variables['zebra'];
  $variables['title_attributes_array']['class'][] = 'comment-title';

  $node_uri = entity_uri('node', $node);
  $comment_uri = entity_uri('comment', $comment);
  $options = array('fragment' => $comment_uri['options']['fragment'], 'attributes' => array('class' => array('permalink'), 'rel' => 'bookmark'));

  $variables['permalink'] = l('#' . $variables['id'], $node_uri['path'], $options);

  $variables['content']['links']['#attributes']['class'][] = $variables['zebra'];

  if (isset($variables['content']['links']['comment']['#links']['comment_forbidden'])) {
    unset($variables['content']['links']['comment']['#links']['comment_forbidden']);
  }
}

/**
 * Preprocess variables for block.tpl.php.
 */
function vmoskve_preprocess_block(&$variables) {
  $variables['title_attributes_array']['class'][] = 'block-title';
  if (empty($variables['block']->subject)) {
    $variables['classes_array'][] = 'no-title';
  }
}

/**
 * Override HTML for a feed icon.
 */
function vmoskve_feed_icon($variables) {
  $text = t('Subscribe to !feed-title', array('!feed-title' => $variables['title']));
  if ($image = theme('image', array('path' => path_to_theme() . '/images/rss.png', 'width' => 42, 'height' => 14, 'alt' => $text))) {
    return l($image, $variables['url'], array('html' => TRUE, 'attributes' => array('class' => array('feed-icon'), 'title' => $text)));
  }
}

/*
 * Output login/register/logout/profile links.
 *
 * @return string
 *   Output Log in and Register links if user is anonymous, otherwise Profile and Logout.
 */
function _vmoskve_user_links() {
  global $user;

  $links = array();
  if ($user->uid) {
    $links[] = l(t('Profile'), 'user');
    $links[] = l(t('Logout'), 'user/logout');
  }
  else {
    if (module_exists('ajax_register')) {
      _ajax_register_include_modal();
      $links = _ajax_register_ajax_links(array('login', 'register'));
    }
    else {
      $links[] = l(t('Login'), 'user/login');
      if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
        $links[] = l(t('Create new account'), 'user/register');
      }
    }
  }

  return theme('item_list', array('items' => $links, 'attributes' => array('class' => array('links', 'inline'))));
}

/*
 * Output today date.
 *
 * @return string
 *   Today date in format l d.m.Y.
 */
function _vmoskve_today_date() {
  return format_date(REQUEST_TIME, 'custom', 'l d.m.Y');
}

/*
 * Output rss and contact icons.
 *
 * @param $rss
 *   A html for rss icon.
 *
 * @return string
 *   Output rss and contact icons.
 */
function _vmoskve_rss_contact($rss) {
  $icons = array();
  if (!empty($rss)) {
    $icons[] = $rss;
  }
  if (module_exists('contact') && user_access('access site-wide contact form')) {
    $image = theme('image', array('path' => path_to_theme() . '/images/contact.png', 'width' => 20, 'height' => 14, 'alt' => t('Contact')));
    $icons[] = l($image, 'contact', array('html' => TRUE, 'attributes' => array('class' => array('contact-icon'), 'title' => t('Contact'))));
  }

  return theme('item_list', array('items' => $icons, 'attributes' => array('class' => array('links', 'inline'))));
}

/*
 * Output a developer's logo.
 *
 * @return string
 *   A developer logo.
 */
function _vmoskve_developer_logo() {
  return l('', 'http://www.internet-marketing.by/', array('attributes' => array('title' => 'Internet-Marketing', 'rel' => 'external', 'id' => 'developer-logo')));
}
