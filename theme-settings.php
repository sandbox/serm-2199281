<?php

/**
 * @file
 * Theme setting callbacks for the vmoskve theme.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function vmoskve_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['vmoskve'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vmoskve settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['vmoskve']['vmoskve_front_page_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Front page title.'),
    '#description' => t('Allow the title to be rendered on the front page.'),
    '#default_value' => theme_get_setting('vmoskve_front_page_title'),
  );

  $form['vmoskve']['vmoskve_front_page_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Front page content.'),
    '#description' => t('Allow the main content block to be rendered on the front page.'),
    '#default_value' => theme_get_setting('vmoskve_front_page_content'),
  );

  $form['vmoskve']['vmoskve_page_breadcrumbs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Page breadcrumbs.'),
    '#description' => t('Allow the breadcrumbs to be rendered on the page.'),
    '#default_value' => theme_get_setting('vmoskve_page_breadcrumbs'),
  );

  $copyright = theme_get_setting('vmoskve_copyright');
  $form['vmoskve']['vmoskve_copyright'] = array(
    '#type' => 'text_format',
    '#title' => t('Copyright'),
    '#default_value' => $copyright['value'],
    '#format' => $copyright['format'],
  );
}
